<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'URLify' => array($vendorDir . '/jbroadway/urlify'),
    'OAuth2' => array($vendorDir . '/bshaffer/oauth2-server-php/src'),
    'HTMLPurifier' => array($vendorDir . '/ezyang/htmlpurifier/library'),
    '' => array($vendorDir . '/kzykhys/git/src'),
);
