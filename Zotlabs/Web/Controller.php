<?php

namespace Zotlabs\Web;

/*
 * Base class for controllers
 *
 */

class Controller
{

    public function init()
    {
    }

    public function post()
    {
    }

    public function get()
    {
    }
}
