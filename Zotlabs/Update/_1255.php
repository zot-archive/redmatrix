<?php

namespace Zotlabs\Update;

use Zotlabs\Lib\Config;

class _1255
{

    // Dropping the following deprecated database entries will make exports and dumps
    // importable into streams, which contains a number of additional DB optimisations

    public function run()
    {
        q("ALTER table abook drop column abook_my_perms");
        q("ALTER table abook drop column abook_their_perms");
        q("DROP table auth_codes");
        q("DROP table conv");
        q("DROP table issue");
        q("DROP table mail");
        q("DROP table shares");
        q("DROP table sys_perms");
        q("DROP table tokens");
        q("DROP table vote");
        return UPDATE_SUCCESS;
    }

    public function verify()
    {
        return true;
    }
}